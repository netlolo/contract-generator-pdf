import JSZip from 'jszip';
import Docxtemplater from 'docxtemplater';
import ImageModule from 'open-docxtemplater-image-module';
import { promisify } from 'util';
import fs from 'fs';
import ErrorDocxPdf from './../../lib/ErrorDocxPdf';

class Docx {
  constructor() {
    const readFileAsync = promisify(fs.readFile);
    this.readFileAsync = readFileAsync;
    this.opts = {
      getImage: image => image,
      getSize: () => [320, 40],
      centered: false,
    };
  }

  setDocx(file) {
    return this.readFileAsync(file, { encoding: 'binary' })
      .then((res) => {
        const zip = new JSZip(res);
        const docx = new Docxtemplater();
        this.docx = docx.loadZip(zip);
      });
  }

  makeData(data) {
    if (typeof data !== 'object') {
      const err = new ErrorDocxPdf('makeData aceita apenas objeto!', 400, 'ZTX_DOC2PDF_003');
      return Promise.reject(err);
    }
    const imageModule = new ImageModule(this.opts);
    return this.docx.setData(data).attachModule(imageModule);
  }

  async render() {
    try {
      await this.docx.render();
      return true;
    } catch (e) {
      const err = new ErrorDocxPdf(e.message, 500, 'ZTR_DOC2PDF_002');
      return Promise.reject(err);
    }
  }

  async makeContract(file, data) {
    try {
      const resol = await Promise.resolve(data).then(res => res);

      await this.setDocx(file);
      await this.makeData(resol);
      await this.render();
      return this.docx.getZip()
        .generate({ type: 'nodebuffer' });
    } catch (e) {
      return Promise.reject(e);
    }
  }
}

export default () => new Docx();
