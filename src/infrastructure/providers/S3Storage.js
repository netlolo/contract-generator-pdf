import AWS from 'aws-sdk';
import { readFile, unlink } from 'fs';
import StorageDecorator from './../../decorators/storageDocorator';

class S3Storage extends StorageDecorator {
  constructor(config) {
    super();
    this.config = config;
    AWS.config.update({
      accessKeyId: config.AWS_ACCESS_KEY_ID,
      secretAccessKey: config.AWS_SECRET_KEY,
      region: config.AWS_REGION,
    });
  }

  setBucket(bucket) {
    this.bucket = bucket;
  }

  setFile(file) {
    this.file = file;
  }

  getFileData() {
    return new Promise((resolve, reject) => {
      if (this.fileBuffer) {
        resolve(this.fileBuffer.data);
      } else {
        readFile(this.file, (err, fileData) => {
          if (err) {
            reject(err);
          } else {
            resolve(fileData);
          }
        });
      }
    });
  }

  getFileType() {
    if (this.file) return this.file.type;
    if (this.fileBuffer) return this.fileBuffer.type;
    return null;
  }


  async upload() {
    AWS.config.update({ region: this.config.AWS_REGION });
    return new Promise(async (resolve, reject) => {
      if (!this.destinationDir) {
        reject('No bucket provided');
        return;
      }

      if (!this.file && !this.fileBuffer) {
        reject('No file provided.');
        return;
      }

      try {
        const fileData = await this.getFileData();
        
        const s3bucket = new AWS.S3({
          params: { Bucket: this.bucket },
        });

        const fileInfo = this.getFinalFilePath();        
        s3bucket.createBucket(() => {
          const s3Params = {
            Key: fileInfo.fullPath,
            ContentType: this.getFileType(),
            Body: fileData,
            ACL: 'public-read',
          };
          s3bucket.upload(s3Params, (uploadErr, uploadData) => {
            if (this.file) {
              unlink(this.file, (err) => { if (err) throw new Error(err);});
            }

            if (uploadErr) {
              return reject(uploadErr);
            }

            return resolve({ ...uploadData, file: fileInfo.fileName });
          });
        });
      } catch (e) {
        reject(e);
      }
    });
  }
}

export default config => new S3Storage(config);
