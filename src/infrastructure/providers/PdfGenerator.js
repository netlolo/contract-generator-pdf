const fs = require('fs');
const pdf = require('dynamic-html-pdf');
const bufferConverter = require('arraybuffer-to-string');
const path = require('path');
const ContractType = require('../../domain/ContractTypeEnum');
const Signatures = require('../../domain/Signature');

class PdfGenerator {
    constructor(type) {
        this.type = type;
    }

    async generate(contract, file_name, insertSignature) {
        let resol = await Promise.resolve(contract).then(res => res);
        resol.image = 'data:image/png;base64,' + bufferConverter(resol.image, 'base64');
        if (insertSignature){
            resol = await this.makeSignatures(resol);
        }
        //adding this property to get "base directory to the header images" 
        resol.basePath = path.join('file:///', __dirname, '../../../assets/');

        return new Promise((resolve, reject) => {
            try {
                let html = '';
                if (this.type == ContractType.MUTUO_TRADING)
                    html = fs.readFileSync(path.join(__dirname, '../../../assets/MutuoComTrading.html'), 'utf8');

                if (this.type == ContractType.MUTUO_SEM_TRADING)
                    html = fs.readFileSync(path.join(__dirname, '../../../assets/MutuoSemTrading.html'), 'utf8');

                if (this.type == ContractType.MUTUO_LE_REVE)
                    html = fs.readFileSync(path.join(__dirname, '../../../assets/MutuoComProduto.html'), 'utf8');

                const footer = this.buildFooter();
                const options = {
                    format: 'Letter',
                    base: path.join('file:///', __dirname, '../../../assets/'),
                    header: {
                        contents: "",//header
                        height: "35mm"
                    },
                    footer: {
                        "contents": footer
                    }
                };

                const document = {
                    type: 'file', // 'file' or 'buffer'
                    template: html,
                    context: {
                        contract: resol
                    },
                    path: './' + file_name + '.pdf'  // it is not required if type is buffer
                };

                resolve(pdf.create(document, options));

            } catch (err) {
                reject(err);
            }
        });
    }

    async makeSignatures(contract) {
        contract.vpSignature = Signatures.HELOISA;
        contract.comercialSignature = Signatures.JAIME;
        const firstSignature = await this.randomizeSignature();
        let secondSignature = await this.randomizeSignature();
        while (firstSignature === secondSignature) {
            secondSignature = await this.randomizeSignature();
        }
        contract.firstRandomSignature = firstSignature;
        contract.secondRandomSignature = secondSignature;
        return contract;
    }

    async randomizeSignature() {
        const testimonials = Object.keys(Signatures).filter(function(key) {
            if (Signatures[key] !== Signatures.HELOISA && Signatures[key] !== Signatures.JAIME)
              return Signatures[key];
          });
          return Signatures[testimonials[Object.keys(testimonials)[Math.floor(Math.random() * Object.keys(testimonials).length)]]];
    }

    buildFooter() {
        if (this.type == 1)
        {
            return `
                <p class="c3">
                    <span class="c4 c8">Alameda Doutor Carlos de Carvalho n. 417 &ndash; 30&ordm; andar. Conjunto 3001 &nbsp;| &nbsp;Bairro Centro &nbsp;|
                        &nbsp;CEP 80410-180 &nbsp;| &nbsp;Curitiba . Paran&aacute;</span>
                </p>
                <p class="c3">
                    <span class="c4 c8">Telefone +55 41 3123-9192</span>
                </p>
                
                <p class="c3">
                    <span class="c8 c14"></span>
                    <table class="c21">
                            <tbody>
                                <tr class="c45">
                                    <td class="c35" colspan="1" rowspan="1">
                                        <p class="c6">
                                            <span class="c4 c0">Mutuo Com Trading v-4.3 . 26062018</span>
                                        </p>
                                    </td>
                                    <td class="c62" colspan="1" rowspan="1">
                                        <p class="c3">
                                            <span class="c0 c40">P&aacute;gina {{page}} </span>
                                            <span class="c0 c40">&nbsp;de {{pages}} </span>
                                        </p>
                                    </td>
                                    <td class="c51" colspan="1" rowspan="1">
                                        <p class="c3 c16">
                                            <span class="c4 c0"></span>
                                        </p>
                                    </td>
                                    <td class="c64" colspan="1" rowspan="1">
                                        <p class="c3">
                                            <span class="c4 c0">Dr. Ismair Junior Couto</span>
                                        </p>
                                        <p class="c3">
                                            <span class="c4 c0">Diretor Jur&iacute;dico</span>
                                        </p>
                                        <p class="c3">
                                            <span class="c4 c0">OAB/PR 49001</span>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                </p>
                <a id="t.082bc7f9bd17458e7662efd45871e7b72a4a82ff"></a>
                <a id="t.6"></a>
                    
                <p class="c3 c16">
                    <span class="c14 c8"></span>
                </p>`;
        }
        else
        if (this.type == 2)
        {
            return `            
                <p class="c2">
                    <span class="c18 c5 c8">Alameda Doutor Carlos de Carvalho n. 417 &ndash; 30&ordm; andar. Conjunto 3001 &nbsp;| &nbsp;Bairro Centro &nbsp;|
                        &nbsp;CEP 80410-180 &nbsp;| &nbsp;Curitiba . Paran&aacute;</span>
                </p>
                <p class="c2">
                    <span class="c18 c5 c8">Telefone +55 41 3123-9192</span>
                </p>
                <p class="c2 c3">
                    <span class="c18 c5 c53"></span>
                </p>
                <a id="t.d52fbb6362b5826fe79fb1784747727bdf6992f8"></a>
                <a id="t.6"></a>
                <table class="c22">
                    <tbody>
                        <tr class="c21">
                            <td class="c39" colspan="1" rowspan="1">
                                <p class="c7">
                                    <span class="c18 c0 c8">Mutuo Sem Trading v-4.3 . 26062018</span>
                                </p>
                            </td>
                            <td class="c54" colspan="1" rowspan="1">
                                <p class="c2">
                                    <span class="c0 c26">P&aacute;gina {{page}} </span>
                                    <span class="c0 c26">&nbsp;de {{pages}} </span>
                                </p>
                            </td>
                            <td class="c10" colspan="1" rowspan="1">
                                <p class="c2 c3">
                                    <span class="c18 c0 c8"></span>
                                </p>
                            </td>
                            <td class="c47" colspan="1" rowspan="1">
                                <p class="c2">
                                    <span class="c18 c0 c8">Dr. Ismair Junior Couto</span>
                                </p>
                                <p class="c2">
                                    <span class="c18 c0 c8">Diretor Jur&iacute;dico</span>
                                </p>
                                <p class="c2">
                                    <span class="c18 c0 c8">OAB/PR 49001</span>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="c2 c3">
                    <span class="c18 c5 c53"></span>
                </p>            
            `;
        }
        else
        if (this.type == 3)
        {
            return `
                <span class="c4 c8">Alameda Doutor Carlos de Carvalho n. 417 &ndash; 30&ordm; andar. Conjunto 3001 &nbsp;| &nbsp;Bairro Centro &nbsp;|
                    &nbsp;CEP 80410-180 &nbsp;| &nbsp;Curitiba . Paran&aacute;</span>
            </p>
            <p class="c3">
                <span class="c4 c8">Telefone +55 41 3123-9192</span>
            </p>
            
            <p class="c3">
                <span class="c8 c14"></span>
                <table class="c21">
                        <tbody>
                            <tr class="c45">
                                <td class="c35" colspan="1" rowspan="1">
                                    <p class="c3">
                                        <span class="c0 c40">Produto</span>
                                        <span class="c0 c40">Mútuo Le Rêve</span>
                                    </p>
                                </td>
                                <td class="c62" colspan="1" rowspan="1">
                                    <p class="c3">
                                        <span class="c0 c40">P&aacute;gina {{page}} </span>
                                        <span class="c0 c40">&nbsp;de {{pages}} </span>
                                    </p>
                                </td>
                                <td class="c51" colspan="1" rowspan="1">
                                    <p class="c3">
                                        <span class="c4 c0">Vers&atilde;o</span>
                                    </p>
                                    <p class="c3">
                                        <span class="c4 c0">1.4</span>
                                    </p>
                                </td>
                                <td class="c64" colspan="1" rowspan="1">
                                    <p class="c3">
                                        <span class="c4 c0">Dr. Ismair Junior Couto</span>
                                    </p>
                                    <p class="c3">
                                        <span class="c4 c0">Diretor Jur&iacute;dico</span>
                                    </p>
                                    <p class="c3">
                                        <span class="c4 c0">OAB/PR 49001</span>
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </p>
            <a id="t.082bc7f9bd17458e7662efd45871e7b72a4a82ff"></a>
            <a id="t.6"></a>
                
            <p class="c3 c16">
                <span class="c14 c8"></span>
            </p>`;
        }
    }
}

export default (type) => new PdfGenerator(type);
