import ErrorDocxPdf from '../lib/ErrorDocxPdf';

export default class ContractDecorator {
  setType(type) {
    this.type = type;
  }

  setDigitalData(DigitalData) {
    this.DigitalData = DigitalData;
  }

  save(userId, contractInfo) {
    try {
      const digitalData = { user_id: userId, type: this.type, origin: 'btc-banco-s3', data: contractInfo };
      return Promise.resolve(this.DigitalData.create(digitalData))
        .then(result => ({ digitaliId: result.id, ...contractInfo }))
        .catch((e) => {
          throw e;
        });
    } catch (e) {
      const err = new ErrorDocxPdf('Contrato não criado', 500, 'ZTR_DOCPDF_004');
      return Promise.reject(err);
    }
  }

  update(digitalData, id) {
    try {
      if (!id) throw new Error('Contrato não encontrado: digitalData id nulo');

      return Promise.resolve(this.DigitalData.findOne({ where: { id: id } })).then(result => {
        if(result.data.contractUrlAssignature !== undefined){
          digitalData['contractUrlAssignature'] = result.data.contractUrlAssignature
        }
        result.data = digitalData;
        return result.updateAttributes(result);
      }).catch(e => {
        throw e;
      });
    } catch (e) {
      const err = new _ErrorDocxPdf2.default('Contrato não alterado', 500, 'ZTR_DOCPDF_004');
      return Promise.reject(err);
    }
  }

  getByUserId(userId) {
    try {
      return Promise.resolve(this.DigitalData.find({ where: { user_id: userId, type: this.type } }))
        .then(res => res)
        .catch((e) => {
          throw e;
        });
    } catch (e) {
      const err = new ErrorDocxPdf('Nenhum contrato encontrado', 400, 'ZTR_DOCPDF_005');
      return Promise.reject(err);
    }
  }

  async make() { //eslint-disable-line
    return Promise.reject('Not implemented yet.');
  }

  generatorLast() 
  {
    return Promise.resolve(this.getAll()).then((res) => {
      const d = new Date();
      const y = d.getFullYear();
      if (res.length === 0) return `${y}0001`;
      let total = res.length + 1;
      console.log('Numero de Contrato')
      console.log(total)
      //total = total + 1000;
      return `${y}${(`0000${total}`).slice(-4)}`;
    });
  }
  getAll() {
    try {
      return Promise.resolve(this.DigitalData.find({ where: { type: this.type } }))
        .then(res => res)
        .catch((e) => {
          throw e;
        });
    } catch (e) {
      const err = new ErrorDocxPdf('Nenhum contrato encontrado', 400, 'ZTR_DOCPDF_006');
      return Promise.reject(err);
    }
  }
}
