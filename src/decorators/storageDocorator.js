import uuid from 'uuid';

export default class StorageDecorator {
  setFile(file) {
    this.file = file;
  }

  setFileBuffer(fileBuffer) {
    this.fileBuffer = fileBuffer;
  }

  setDestinationDir(dir) {
    this.destinationDir = dir;
  }

  setOptions(options) {
    this.options = options;
  }

  async upload() { //eslint-disable-line
    return Promise.reject('Not implemented yet.');
  }

  getFinalFilePath() {
    let ext;

    if (this.file) {
      const tmpExt = ((this.file || {}).name || '').split('.');
      ext = 'pdf';
    } else if (this.fileBuffer) {
      ext = this.fileBuffer.type.split('/')
        .pop();
    }

    const newFileName = `${uuid.v4()}.${ext}`;
    const newFilePath = `${this.destinationDir}/${newFileName}`;

    return { fullPath: newFilePath, fileName: newFileName };
  }
}
