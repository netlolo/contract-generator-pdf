export default class ErrorCep extends Error {
  constructor(message, status = 500, i18n = 'ZTR_CEP_001') {
    super(message);
    this.name = i18n;
    this.status = status;
    this.i18n = i18n || 'ZTR_CEP_001';
    this.code = status || 500;
  }
}
