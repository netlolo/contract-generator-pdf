export default class ErrorDocxPdf extends Error {
  constructor(message, status = 500, i18n = 'ZTR_DOC2PDF_001') {
    super(message);
    this.name = i18n;
    this.status = status;
    this.i18n = i18n || 'ZTR_DOC2PDF_001';
    this.code = status || 500;
  }
}
