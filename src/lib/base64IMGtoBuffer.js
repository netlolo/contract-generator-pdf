module.exports = (dataURL) => {

  const stringBase64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
  const binaryString = new Buffer(stringBase64, 'base64').toString('binary');
  const len = binaryString.length;
  const bytes = new Uint8Array(len);

  for (let i = 0; i < len; i++) {
    const ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes.buffer;
};
