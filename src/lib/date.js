module.exports = () => {
  const date = new Date();
  const daySemana = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira',
    'Sábado'][date.getDay()];
  const day = date.getDate();
  const month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
    'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'][date.getMonth()];
  const year = date.getFullYear();
  const dateObj = {
    daySemana,
    month,
    year,
    day,
  };
  return dateObj;
}
