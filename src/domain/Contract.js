import ErrorDocxPdf from '../lib/ErrorDocxPdf';
import path from 'path';
import dateFormat from '../lib/date';
import base64IMGBuffer from '../lib/base64IMGtoBuffer';
import ContractDecorator from '../decorators/ContractDecorator';
import fetch from 'node-fetch';
import moment from 'moment';
const numeroPorExtenso = require('numero-por-extenso');
const CPF = require('cpf');

class Contract extends ContractDecorator {
	constructor(Docx, DigitalData, S3Storage, PdfGenerator) {
		super();
		this.Storage = S3Storage;
		this.Docx = Docx;
		this.PdfGenerator = PdfGenerator;
		this.setDigitalData(DigitalData);
	}

	setConfig(config) {
		this.config = config;
		this.Storage = this.Storage(config);
	}

	async makeCPF(data) {
		const cotacao = await fetch('http://icoinomia.com.br/api/CotacaoCrypto');
    let cotacaoJson = await cotacao.json();   
    
    
    
    switch (data.coin_id) {
    	case "-LTIsI_I5BwSOhFpGsUC":
    		cotacaoJson = cotacaoJson.cotacao.find(row => row.Codigo === 'LEAXBRL').Valor;
    		break;
    	case "-L8lxvN2Md2BthLFjMYp":
	    	cotacaoJson = cotacaoJson.cotacao.find(row => row.Codigo === 'BTCBRL').Valor;
	    	break;
    	default:
    		cotacaoJson = cotacaoJson.cotacao.find(row => row.Codigo === 'BTCBRL').Valor;
    		break;
    }

    var clientWalletByCoin = data.clientWallet.filter(function(row){
    	if(row.coin == data.coin_id)
    		return row
    })

    console.log('***********************')
    console.log(data)
    console.log(clientWalletByCoin)
    console.log('***********************')

	  const calculateValue = () => {
			if (
				typeof parseFloat(data.amountCrypt) !== 'number' ||
				isNaN(parseFloat(data.amountCrypt))
			)
				throw "It's not a float amount Object!";

			if (
				typeof parseFloat(cotacaoJson) !== 'number' ||
				isNaN(parseFloat(cotacaoJson))
			)
				throw "It's not a float money Object";

			const calc = parseFloat(data.amountCrypt) * parseFloat(cotacaoJson);

			return parseFloat(calc);
		};

		const val = numeroPorExtenso.porExtenso(
			calculateValue(),
			numeroPorExtenso.estilo.monetario
		);
		const formato = {
			minimumFractionDigits: 2,
			style: 'currency',
			currency: 'BRL'
		};
		const amountMoney = `${calculateValue().toLocaleString('pt-BR', formato)} (${val})`;

		const contract = {
			contractCoin: data.coins[0],
			investmentId: data.id,
			type: data.type,
			duration: data.duration,
			penaltyPorcent: data.penaltyPorcent || 0,
			penaltyAdv: data.penaltyAdv || 0,
			clientName: data.clientName || 'Daniel Silva Miranda',
			productName: data.product || 'Produto de Testes',
			managerName: data.managerName || 'Daniel Silva Miranda',
			contractNumber: data.contractNumber || 'Exemplo',
			contractEnd: data.contractEnd || '00/00/0000',
			contractTaxe: data.contractTaxe || 0,
			contractTrade: data.contractTrade || 0,
			amountCrypt: data.amountCrypt || 0,
			contractPorcent: data.contractPorcent || 0,
			contractPorcentTotal: data.contractPorcentTotal || 0,
			bankName: data.bank.name.split('#')[1] || 'BTC Banco',
			bankTitular: data.bank.userName || 'BTC Banco',
			bankAgency: data.bank.agency || '0000',
			bankAccount: data.bank.account || '0000-00',
			bankCode: data.bank.code || '0000-00',
			// eslint-disable-next-line
			image: base64IMGBuffer(data.image),
			vpSignature:
				'data:image/png;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
			comercialSignature:
				'data:image/png;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
			firstRandomSignature:
				'data:image/png;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
			secondRandomSignature:
				'data:image/png;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
			amountMoney,
			...dateFormat(),
			clientCPF: `${data.clientCPF ? CPF.format(data.clientCPF) : ''}`,
			date: moment(new Date()).format('DD/MM/YYYY'),
			clientWallet: clientWalletByCoin[0].data.address || '1CfUYUVsc1gb9caCDQjwTwQ8jDY3B1ZRU3',
			clientResume:
				`${data.clientName}, natural de ${data.clientNationality}, ${
					data.clientCivilState
				},
      ${data.clientOccupation},
      portador do RG nº ${data.clientRG}, inscrito no CPF sob o nº ${
					data.clientCPF ? CPF.format(data.clientCPF) : ''
				}, residente e 
      domiciliado a
      ${data.clientStreet}
      ${data.clientComplement || ''}, CEP ${data.clientCEP}, ${
					data.clientCity
				} ${data.clientState}` || ' '
		};
		return contract;
	}

	async makeCNPJ(data) {
    const result = await this.makeCPF(data);
    result.clientName = data.socialReason
		const contract = {
			...result,
			clientResume: `${
				data.socialReason
			}, pessoa jurídica de direito privado, inscrita no CNPJ nº ${
				data.clientCNPJ
			}, com sede na ${data.companyStreet}${
				data.companyComplement ? ' ' + data.companyComplement : ''
			}, CEP ${data.companyCEP}, ${data.companyCity}, ${
				data.companyState
			}, representado por ${data.partnerClientName}, ${data.partnerCivilState}${
				data.partnerProfession != '' ? ', ' + data.partnerProfession : ''
			}, portador do RG nº ${
				data.partnerPersonalRegister
			}, inscrito no CPF sob o nº ${CPF.format(
				data.partnerPersonalDocument
			)}, residente e domiciliado a ${data.partnerAddress} ${
				data.partnerAddressComplement ? ' ' + data.partnerAddressComplement : ''
			}, CEP ${data.partnerPostalCode}`
		};
		return contract;
	}

	async make(userId, dataContract, config) {
		
		try {
			await this.setConfig(config.aws);
			await this.setType(config.contract);
			dataContract.contractNumber = await this.generatorLast();
			if (this.config === undefined)
				throw new ErrorDocxPdf('Configuração não inicializada');
			let data;
			// let file;

			if (dataContract.clientCNPJ !== undefined) {
				data = await this.makeCNPJ(dataContract);
				// file = path.resolve(__dirname, './../../assets/docEx.docx');
			} else {
				data = await this.makeCPF(dataContract);
				// file = path.resolve(__dirname, './../../assets/docEx.docx');
			}

			await this.PdfGenerator.generate(data, userId, false);
			return (
				Promise.resolve(this.Storage.setBucket(config.bucket))
					.then(() => this.Storage.setDestinationDir('contracts'))
					// .then(() => this.Docx.makeContract(file, data))
					// .then(() => this.PdfGenerator.generate(data))
					// .then(res => Buffer.from(res, 'base64'))
					// .then(res => this.Storage.setFileBuffer({ data: res, type: 'pdf' }))
					.then(() => this.Storage.setFile(`./${userId}.pdf`))
					.then(() => this.Storage.upload())
					.then(res => JSON.stringify(res))
					.then(res => JSON.parse(res))
					.then(res =>
						Promise.resolve(data).then(result => ({
							...result,
							contractUrl: res.Location,
							status: 'Ativo'
						}))
					)
					.then(res => this.save(userId, res))
					.catch(e => {
						throw e;
					})
			);
		} catch (e) {
			return Promise.reject(e);
		}
	}

	async view(dataContract, config) {
		//console.log(dataContract)

		try {
			await this.setConfig(config.aws);
			await this.setType(config.contract);
			if (this.config === undefined)
				throw new ErrorDocxPdf('Configuração não inicializada');

			const data = dataContract;

			await this.PdfGenerator.generate(data, data.clientId, false);
			return Promise.resolve(this.Storage.setBucket(config.bucket))
				.then(() => this.Storage.setDestinationDir('contracts'))
				.then(() => this.Storage.setFile(`./${data.clientId}.pdf`))
				.then(() => this.Storage.upload())
				.then(res =>
					Promise.resolve(data).then(result => ({
						...result,
						contractUrl: res.Location,
						status: 'Ativo'
					}))
				)
				.then(res => this.update(res))
				.catch(e => {
					throw e;
				});
		} catch (e) {
			return Promise.reject(e);
		}
	}

	async insertSignature(userId, dataContract, config) {
		try {
			await this.setConfig(config.aws);
			await this.setType(config.contract);
			if (this.config === undefined)
				throw new ErrorDocxPdf('Configuração não inicializada');
			//let file;
			// if (dataContract.clientCNPJ !== undefined) {
			//   file = path.resolve(__dirname, './../../assets/docEx.docx');
			// } else {
			//   file = path.resolve(__dirname, './../../assets/docEx.docx');
			// }
			const data = this.makeDocument(dataContract.data);
			await this.PdfGenerator.generate(data, userId, true);
			return (
				Promise.resolve(this.Storage.setBucket(config.bucket))
					.then(() => this.Storage.setDestinationDir('contracts'))
					//.then(() => this.Docx.makeContract(file, dataContract.data))
					//.then(res => Buffer.from(res, 'base64'))
					//.then(res => this.Storage.setFileBuffer({ data: res, type: 'docx' }))
					.then(() => this.Storage.setFile('./' + userId + '.pdf'))
					.then(() => this.Storage.upload())
					.then(res => JSON.stringify(res))
					.then(res => JSON.parse(res))
					.then(aws =>
						Promise.resolve(
							data.then(result => {
								return { aws, result };
							})
						)
					)
					.then(res => {
						res.result.contractUrl = res.aws.Location;
						res.result.status = 'Assinado';
						return res.result;
					})
					.then(res => this.update(res, dataContract.id))
					.catch(e => {
						throw e;
					})
			);
		} catch (e) {
			return Promise.reject(e);
		}
	}

	async makeDocument(dataContract) {
		if (dataContract.clientCNPJ !== undefined) {
			return await this.makeCNPJ(dataContract);
			//file = path.resolve(__dirname, './../../assets/docEx.docx');
		} else {
			return await this.makeCPF(dataContract);
			//file = path.resolve(__dirname, './../../assets/docEx.docx');
		}
	}
}

module.exports = (Docx, DigitalData, Storage, PdfGenerator) =>
	new Contract(Docx, DigitalData, Storage, PdfGenerator);
