// const Library = require('./dist/src/infrastructure/providers/PdfGenerator');
// const base64IMGBuffer = require ('./dist/src/lib/base64IMGtoBuffer');
// const Signatures = require('./dist/src/domain/Signature');
// const numeroPorExtenso = require('numero-por-extenso');
// function TestPdf() {    
//     Library.default(2).generate(contract(), 'teste');
// }

// async function contract(){
//     var x = await randomizeSignature();
//     var y = await randomizeSignature();
//     while(x === y){
//         y = await randomizeSignature();
//     }
//     const cotacao = 2934703;
//     const valor = numeroPorExtenso.porExtenso(parseFloat(cotacao), numeroPorExtenso.estilo.monetario);

//     let contract = { 
//         contractNumber: 123123,
//         date:'11/07/2018',
//         clientResume: 'Teste Teste Teste Teste Teste Teste Teste',
//         contractEnd: '21/12/2020',
//         amountCrypt: 10,
//         amountMoney: `${cotacao.toLocaleString(undefined, {style: 'currency', currency: 'BRL' })} (${valor})`,
//         bankAgency: '123',
//         bankAccount: '1233-1',
//         bankTitular: 'Teste Teste Teste',
//         contractTrade: '5',
//         day: '11',
//         month: 'julho',
//         year: '2018',
//         firstRandomSignature: x,
//         secondRandomSignature: y,
//         image: base64IMGBuffer('data:image/png;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7') 
//     };
//     contract = await makeSignatures(contract);
//     return contract;
// }

// async function makeSignatures(contract) {
//     contract.vpSignature = Signatures.HELOISA;
//     contract.comercialSignature = Signatures.JAIME;
//     var firstSignature = await randomizeSignature();
//     let secondSignature = await randomizeSignature();
//     while (firstSignature === secondSignature) {
//         secondSignature = await randomizeSignature();
//     }
//     contract.firstRandomSignature = firstSignature;
//     contract.secondRandomSignature = secondSignature;
//     return contract;
// }
//   async function randomizeSignature(){
//     const testimonials = Object.keys(Signatures).filter(function(key) {
//       if (Signatures[key] !== Signatures.HELOISA && Signatures[key] !== Signatures.JAIME)
//         return Signatures[key];
//     });
//     return Signatures[testimonials[Object.keys(testimonials)[Math.floor(Math.random() * Object.keys(testimonials).length)]]];
//   }

// function randomizeSignature(){
//     return Signatures[Object.keys(Signatures)[Math.floor(Math.random() * Object.keys(Signatures).length)]];
// }

// TestPdf();

const fetch = require('node-fetch');

var data = {};
data.contractTrade = 0.40000000;

const cotacao = fetch('http://icoinomia.com.br/api/CotacaoCrypto')
      .then(res => res.json())
      .then(json => json.cotacao.filter( row => row.Codigo == 'BTCBRL'))
      .then(value => value[0].Valor)
      .then(value => ( data.contractTrade ) ? parseFloat(data.contractTrade) * parseFloat(value) : value  )
      .then(data => console.log(data));

//console.log(cotacao)
