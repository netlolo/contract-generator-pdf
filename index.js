import S3Storage from './src/infrastructure/providers/S3Storage';
import Docx from './src/infrastructure/providers/Docx';
import PdfGenerator from './src/infrastructure/providers/PdfGenerator';
import Contract from './src/domain/Contract';

module.exports = (DigitalData, Type) => Contract(Docx, DigitalData, S3Storage, PdfGenerator(Type));
